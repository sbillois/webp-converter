const express = require('express');
const multer = require('multer');
const Jimp = require('jimp');
const fs = require('fs');
const archiver = require('archiver');

const app = express();
const upload = multer({ dest: 'uploads/' });

// Page d'accueil
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// Endpoint de gestion du téléchargement des images
app.post('/upload', upload.array('images', 10), async (req, res) => {
    try {
        // Vérifier s'il y a des fichiers téléchargés
        if (!req.files || req.files.length === 0) {
            return res.status(400).send('Aucune image téléchargée.');
        }

        const images = req.files;

        // Créer un dossier temporaire pour stocker les images converties
        const tempFolder = 'temp/';
        if (!fs.existsSync(tempFolder)) {
            fs.mkdirSync(tempFolder);
        }

        const convertedImages = [];

        // Convertir chaque image au format WebP
        for (let i = 0; i < images.length; i++) {
            const image = images[i];
            const filePath = image.path;
            const fileName = image.filename + '.webp';
            const fileDestination = tempFolder + fileName;

            // Utiliser Jimp pour convertir l'image
            const jimpImage = await Jimp.read(filePath);
            await jimpImage.writeAsync(fileDestination);

            convertedImages.push(fileDestination);
        }

        // Créer un fichier ZIP pour stocker les images converties
        const zipFile = 'converted_images.zip';
        const output = fs.createWriteStream(zipFile);
        const archive = archiver('zip', {
            zlib: { level: 9 } // Utiliser la meilleure compression
        });

        // Ajouter les images converties au fichier ZIP
        for (let i = 0; i < convertedImages.length; i++) {
            const imagePath = convertedImages[i];
            const imageName = 'image' + (i + 1) + '.webp';

            archive.append(fs.createReadStream(imagePath), { name: imageName });
        }

        archive.pipe(output);
        archive.finalize();

        // Fournir un lien de téléchargement pour le fichier ZIP
        output.on('close', () => {
            res.download(zipFile, (err) => {
                // Supprimer les fichiers temporaires et le fichier ZIP après le téléchargement
                fs.unlinkSync(zipFile);
                for (const imagePath of convertedImages) {
                    fs.unlinkSync(imagePath);
                }
            });
        });
    } catch (err) {
        console.error(err);
        res.status(500).send('Une erreur est survenue lors de la conversion des images.');
    }
});

// Démarrer le serveur
app.listen(3000, () => {
    console.log('Serveur démarré sur le port 3000');
});
